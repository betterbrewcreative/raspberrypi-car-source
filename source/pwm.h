#define ADDRESS (0x40)

#define __SUBADR1             (0x02)
#define __SUBADR2             (0x03)
#define __SUBADR3             (0x04)
#define __MODE1               (0x00)
#define __PRESCALE            (0xFE)
#define __LED0_ON_L           (0x06)
#define __LED0_ON_H           (0x07)
#define __LED0_OFF_L          (0x08)
#define __LED0_OFF_H          (0x09)
#define __ALLLED_ON_L         (0xFA)
#define __ALLLED_ON_H         (0xFB)
#define __ALLLED_OFF_L        (0xFC)
#define __ALLLED_OFF_H        (0xFD)

void pwm_init();
void set_pwm_freq(int freq);
void set_pwm(int channel, int on, int off);
