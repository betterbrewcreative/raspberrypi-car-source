#include "pwm.h"
#include <math.h>
#include <stdint.h>

void pwm_init()
{
    set_pwm_freq(50);
}

void set_pwm_freq(int freq)
{
    float prescaleval = 25000000.0;    // 25MHz
    prescaleval /= 4096.0;       // 12-bit
    prescaleval /= (float)freq;
    prescaleval -= 1.0;
    int prescale = floor(prescaleval + 0.5);

    uint8_t oldmode = 0;
    if (-1 == i2c_read(ADDRESS, __MODE1, &oldmode))
    {
        printf("Failed to read old mode\n");
        return;
    }

    uint8_t newmode = (oldmode & 0x7F) | 0x10;        // sleep
    i2c_write(ADDRESS, __MODE1, newmode);        // go to sleep
    i2c_write(ADDRESS, __PRESCALE, prescale);
    i2c_write(ADDRESS, __MODE1, oldmode);
    usleep(5000);
    i2c_write(ADDRESS, __MODE1, oldmode | 0x80);
}

/*
    channel
      left upper: 0 and 1
        Forward: channel 0 = 0           channel 1 = duty
        Reverse: channel 0 = duty        channel 1 = 0
        Stop:    channel 0 = 4095        channel 1 = 4095
     left lower: 2 and 3
        Forward: channel 2 = duty        channel 3 = 0
        Reverse: channel 2 = 0           channel 3 = duty
        Stop:    channel 2 = 4095        channel 3 = 4095
     right lower: 4 and 5
        Forward: channel 4 = 0           channel 5 = duty
        Reverse: channel 4 = duty        channel 5 = 0
        Stop:    channel 4 = 4095        channel 5 = 4095
     right upper: 6 and 7
        Forward: channel 6 = 0           channel 7 = duty
        Reverse: channel 6 = duty        channel 7 = 0
        Stop:    channel 6 = 4095        channel 7 = 4095
    on = 0
    off = duty
*/
void set_pwm(int channel, int on, int off)
{
    uint8_t registers[] = {__LED0_ON_L, __LED0_ON_H, __LED0_OFF_L, __LED0_OFF_H};
    uint8_t values[] = {on & 0xFF, on >> 8, off & 0xFF, off >> 8};

    for (int i = 0; i < 4; i++)
    {
        uint8_t cur_reg = registers[i] + 4 * channel;
        i2c_write(ADDRESS, cur_reg, values[i]);
    }
}
