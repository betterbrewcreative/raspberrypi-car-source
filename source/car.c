#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include "i2c.h"
#include "motor.h"
#include "servo.h"

#define MOTOR
#define SERVO
#define ULTRASONIC

int file;

int main()
{
    if (!i2c_init())
    {
        printf("Could not init i2c\n");
        return 1;
    }

    pwm_init();

#ifdef MOTOR
    motor_init();

    printf("Forward...");
    move_forward();
    sleep(1);

    printf("Backward...");
    move_backward();
    sleep(1);

    printf("Left...");
    move_left();
    sleep(1);

    printf("Right...");
    move_right();
    sleep(1);

    printf("Stopping...");
    stop();
#endif

#ifdef SERVO
    servo_init();

    const int min_angle = 30;
    const int max_angle = 120;
    const int angle_inc = 10;
    int angle = min_angle;
    int dir = 1;
    int stop = 1;
    while (!stop)
    {
        move_servo(angle);
        usleep(500000);
        if (dir)
        {
            angle += angle_inc;
        }
        else
        {
            angle -= angle_inc;
        }

        if (angle <= min_angle || angle >= max_angle)
        {
            if (angle <= min_angle)
            {
                stop = 1;
            }

            dir = !dir;
        }
    }
#endif

#ifdef ULTRASONIC
    int fd = open("/sys/class/gpio/export", O_WRONLY);
    if (fd == -1) {
        perror("Unable to open /sys/class/gpio/export");
        exit(1);
    }

    if (write(fd, "27", 2) != 2) {
        perror("Error writing to /sys/class/gpio/export");
        exit(1);
    }

    close(fd);

    // Set the pin to be an output by writing "out" to /sys/class/gpio/gpio27/direction

    fd = open("/sys/class/gpio/gpio27/direction", O_WRONLY);
    if (fd == -1) {
        perror("Unable to open /sys/class/gpio/gpio27/direction");
        exit(1);
    }

    if (write(fd, "out", 3) != 3) {
        perror("Error writing to /sys/class/gpio/gpio27/direction");
        exit(1);
    }
    close(fd);


    fd = open("/sys/class/gpio/export", O_WRONLY);
    if (fd == -1) {
        perror("Unable to open /sys/class/gpio/export");
        exit(1);
    }

    if (write(fd, "22", 2) != 2) {
        perror("Error writing to /sys/class/gpio/export");
        exit(1);
    }

    close(fd);

    // Set the pin to be an output by writing "in" to /sys/class/gpio/gpio22/direction

    fd = open("/sys/class/gpio/gpio22/direction", O_WRONLY);
    if (fd == -1) {
        perror("Unable to open /sys/class/gpio/gpio22/direction");
        exit(1);
    }

    if (write(fd, "in", 2) != 2) {
        perror("Error writing to /sys/class/gpio/gpio22/direction");
        exit(1);
    }

    close(fd);

    int ultrasonic_out = open("/sys/class/gpio/gpio27/value", O_WRONLY);
    if (fd == -1) {
        perror("Unable to open /sys/class/gpio/gpio27/value");
        exit(1);
    }

    int ultrasonic_in = open("/sys/class/gpio/gpio22/value", O_RDONLY);
    if (fd == -1) {
        perror("Unable to open /sys/class/gpio/gpio22/value");
        exit(1);
    }

    int count = 0;
    while (count < 3)
    {
        count++;

        printf("Out: 1\n");
        if (write(ultrasonic_out, "1", 1) != 1) {
            perror("Error writing to /sys/class/gpio/gpio27/value");
            exit(1);
        }

        usleep(150);

        printf("Out: 0\n");
        if (write(ultrasonic_out, "0", 1) != 1) {
            perror("Error writing to /sys/class/gpio/gpio27/value");
            exit(1);
        }

        printf("Waiting in: 1\n");
        while (1)
        {
            lseek(ultrasonic_in, 0, SEEK_SET);
            char buf;
            if (read(ultrasonic_in, &buf, 1) != 1)
            {
                printf("Failed to read ultrasonic_in\n");
                continue;
            }

            if (buf == '1')
            {
                printf("In: 1\n");
                break;
            }
        }

        clock_t start, end;
        double cpu_time_used;

        printf("Waiting in: 0\n");
        start = clock();
        while (1)
        {
            lseek(ultrasonic_in, 0, SEEK_SET);
            char buf;
            if (read(ultrasonic_in, &buf, 1) != 1)
            {
                printf("Failed to read ultrasonic_in\n");
                continue;
            }

            if (buf == '0')
            {
                printf("In: 0\n");
                break;
            }
        }
        end = clock();
        cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
        printf("cpu_time_use: %g\n", cpu_time_used);
        sleep(3);
    }

    close(ultrasonic_in);
    close(ultrasonic_out);

    fd = open("/sys/class/gpio/unexport", O_WRONLY);
    if (fd == -1) {
        perror("Unable to open /sys/class/gpio/unexport");
        exit(1);
    }

    if (write(fd, "22", 2) != 2) {
        perror("Error writing to /sys/class/gpio/unexport");
        exit(1);
    }

    if (write(fd, "27", 2) != 2) {
        perror("Error writing to /sys/class/gpio/unexport");
        exit(1);
    }

    close(fd);

#endif

    while (1);

    return 0;
}
