cmake_minimum_required(VERSION 3.16)

set(SRC_FILES
    car.c
    i2c.c
    pwm.c
    motor.c
    servo.c
)

add_executable(car ${SRC_FILES})

target_link_libraries(car m)

if(BUILD_TARGET)
    install(TARGETS car DESTINATION /bin)
endif()
