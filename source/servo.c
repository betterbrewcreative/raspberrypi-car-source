#include "servo.h"
#include "pwm.h"

void servo_init()
{
    set_servo_pulse(8, 1500);
    set_servo_pulse(9, 1500);
}

void set_servo_pulse(int channel, int pulse)
{
    // Sets the Servo Pulse,The PWM frequency must be 50HZ"
    pulse = pulse * 4096 / 20000;        //PWM frequency is 50HZ,the period is 20000us
    set_pwm(channel, 0, pulse);
}

void move_servo(int angle)
{
    int error = 10;
    set_servo_pulse(8, 2500 - (int)((angle + error) / 0.09));
}

