#include "motor.h"
#include "pwm.h"

void motor_init()
{
    i2c_write(ADDRESS, __MODE1, 0x00);
}

void move_forward()
{
    /* Left Upper Forward */
    set_pwm(0, 0, 0);
    set_pwm(1, 0, 2000);

    /* Left Lower Forward */
    set_pwm(2, 0, 2000);
    set_pwm(3, 0, 0);

    /* Right Lower Forward */
    set_pwm(4, 0, 0);
    set_pwm(5, 0, 2000);

    /* Right Upper Forward */
    set_pwm(6, 0, 0);
    set_pwm(7, 0, 2000);
}

void move_backward()
{
    /* Left Upper Reverse */
    set_pwm(0, 0, 2000);
    set_pwm(1, 0, 0);

    /* Left Lower Reverse */
    set_pwm(2, 0, 0);
    set_pwm(3, 0, 2000);

    /* Right Lower Reverse */
    set_pwm(4, 0, 2000);
    set_pwm(5, 0, 0);

    /* Right Upper Reverse */
    set_pwm(6, 0, 2000);
    set_pwm(7, 0, 0);
}

void move_left()
{
    /* Left Upper Reverse */
    set_pwm(0, 0, 500);
    set_pwm(1, 0, 0);

    /* Left Lower Reverse */
    set_pwm(2, 0, 0);
    set_pwm(3, 0, 500);

    /* Right Lower Forward */
    set_pwm(4, 0, 0);
    set_pwm(5, 0, 2000);

    /* Right Upper Forward */
    set_pwm(6, 0, 0);
    set_pwm(7, 0, 2000);
}

void move_right()
{
    /* Left Upper Forward */
    set_pwm(0, 0, 0);
    set_pwm(1, 0, 2000);

    /* Left Lower Forward */
    set_pwm(2, 0, 2000);
    set_pwm(3, 0, 0);

    /* Right Lower Reverse */
    set_pwm(4, 0, 500);
    set_pwm(5, 0, 0);

    /* Right Upper Reverse */
    set_pwm(6, 0, 500);
    set_pwm(7, 0, 0);
}

void stop()
{
    /* Left Upper Stop */
    set_pwm(0, 0, 4095);
    set_pwm(1, 0, 4095);

    /* Left Lower Stop */
    set_pwm(2, 0, 4095);
    set_pwm(3, 0, 4095);

    /* Right Lower Stop */
    set_pwm(4, 0, 4095);
    set_pwm(5, 0, 4095);

    /* Right Upper Stop */
    set_pwm(6, 0, 4095);
    set_pwm(7, 0, 4095);
}
